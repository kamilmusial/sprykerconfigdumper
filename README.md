# Inviqa Config Dumper for Spryker

## Install

Register Command in `src/Pyz/Zed/Console/ConsoleDependencyProvider.php`

```php
        if (Environment::isDevelopment() || Environment::isTesting()) {
            ...
            $commands[] = new ConfigDumpConsole();
        }
```

## Usage:
 ```bash
  config:dump [options] [--] [<filter>]

Arguments:
  filter                Regex filter

Options:
      --origin=ORIGIN   Filter origin
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --no-pre          Will not execute pre run hooks
      --no-post         Will not execute post run hooks
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

```
